package pl.bdarul.ss.adapters;

import lombok.RequiredArgsConstructor;
import org.springframework.web.client.RestClient;
import pl.bdarul.ss.domain.clients.IFirstServiceClient;
import pl.bdarul.ss.domain.dto.UserInfoResponse;

import java.util.Optional;

@RequiredArgsConstructor
class FirstServiceRestClientImpl implements IFirstServiceClient {

    private final RestClient restClient;

    @Override
    public Optional<UserInfoResponse> obtainUserInfo(int userId) {

        UserInfoResponse userInfo = restClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/user")
                        .queryParam("userId", userId)
                        .build())
                .retrieve()
                .body(UserInfoResponse.class);
        return Optional.ofNullable(userInfo);
    }
}
