package pl.bdarul.fs.rest.controller;

import io.restassured.module.mockmvc.specification.MockMvcRequestSpecification;
import io.restassured.response.ResponseOptions;
import org.junit.jupiter.api.Test;
import pl.bdarul.fs.BaseTest;
import pl.bdarul.fs.rest.dto.UserInfoResponse;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserInfoControllerTest extends BaseTest {

    @Test
    void validate_shouldReturnUserInfo() {

        // given
        MockMvcRequestSpecification request = given();

        // when
        ResponseOptions<?> response = given().spec(request)
                .param("userId", 1)
                .get("/user");

        // then
        assertEquals(200, response.statusCode());

        // and
        UserInfoResponse userInfoResponse = response.getBody().as(UserInfoResponse.class);
        assertEquals("Jan", userInfoResponse.firstName());
        assertEquals("Kowalski", userInfoResponse.lastName());
        assertEquals(23, userInfoResponse.age());
    }

    @Test
    void validate_shouldNotReturnUserInfo() {

        // given
        MockMvcRequestSpecification request = given();

        // when
        ResponseOptions<?> response = given().spec(request)
                .param("userId", 2)
                .get("/user");

        // then
        assertEquals(404, response.statusCode());
    }
}
