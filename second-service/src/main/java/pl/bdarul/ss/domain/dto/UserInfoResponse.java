package pl.bdarul.ss.domain.dto;

public record UserInfoResponse(String firstName, String lastName, Integer age) {
}
