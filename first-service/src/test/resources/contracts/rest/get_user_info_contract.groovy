package contracts.rest

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "should return user info"

    request {
        method GET()
        url("/user")
                {
                    queryParameters {
                        parameter("userId", value(consumer(regex("\\d+")), producer(1)))
                    }
                }
    }

    response {
        status OK()
        headers {
            contentType applicationJson()
        }
        body(
                "firstName": value(consumer("Jan"), producer(regex("^[A-Za-z]+\$"))),
                "lastName": value(consumer("Kowalski"), producer(regex("^[A-Za-z]+\$"))),
                "age": value(consumer(23), producer(regex("\\d+")))
        )
    }
}
