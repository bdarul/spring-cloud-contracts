package pl.bdarul.fs.rest.dto;

public record UserInfoResponse(String firstName, String lastName, Integer age) {
}
