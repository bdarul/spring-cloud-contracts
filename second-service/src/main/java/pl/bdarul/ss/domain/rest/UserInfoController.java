package pl.bdarul.ss.domain.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.bdarul.ss.domain.dto.UserInfoResponse;
import pl.bdarul.ss.domain.service.UserService;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
class UserInfoController {

    private final UserService userService;

    @GetMapping("/user")
    ResponseEntity<UserInfoResponse> getUserInfo(@RequestParam Integer userId) {
        UserInfoResponse userInfo = userService.getUserInfo(userId);
        return new ResponseEntity<>(userInfo, OK);
    }
}
