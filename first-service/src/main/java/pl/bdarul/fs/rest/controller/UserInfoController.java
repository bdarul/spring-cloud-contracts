package pl.bdarul.fs.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.bdarul.fs.rest.dto.UserInfoResponse;

import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class UserInfoController {

    @GetMapping("/user")
    ResponseEntity<UserInfoResponse> getUserInfo(@RequestParam Integer userId) {
        if (userId == 1) {
            UserInfoResponse response = new UserInfoResponse("Jan", "Kowalski", 23);
            return new ResponseEntity<>(response, OK);
        } else {
            return new ResponseEntity<>(NOT_FOUND);
        }
    }
}
