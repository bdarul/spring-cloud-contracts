package pl.bdarul.ss.adapters;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClient;
import pl.bdarul.ss.domain.clients.IFirstServiceClient;

@Configuration
class FirstServiceRestClientConfig {

    private static final String FIRST_SERVICE_BASE_URL = "http://localhost:8080";

    @Bean
    IFirstServiceClient firstServiceClient()
    {
        RestClient restClient = RestClient.create(FIRST_SERVICE_BASE_URL);
        return new FirstServiceRestClientImpl(restClient);
    }
}
