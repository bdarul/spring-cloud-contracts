package pl.bdarul.fs.contracts;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0, stubs = {
        "classpath*:/META-INF/**/mappings/**/*.json",
})
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ContractTestBase {

    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    public void setupAll() {
        RestAssuredMockMvc.mockMvc(mockMvc);
    }
}
