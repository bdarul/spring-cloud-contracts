package pl.bdarul.ss.domain.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.bdarul.ss.domain.clients.IFirstServiceClient;
import pl.bdarul.ss.domain.dto.UserInfoResponse;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final IFirstServiceClient firstServiceClient;

    public UserInfoResponse getUserInfo(int userId) {
        Optional<UserInfoResponse> userInfoOptional = firstServiceClient.obtainUserInfo(userId);
        return userInfoOptional.orElseThrow(RuntimeException::new);
    }
}
