package pl.bdarul.ss.domain.clients;

import pl.bdarul.ss.domain.dto.UserInfoResponse;

import java.util.Optional;

public interface IFirstServiceClient {

    Optional<UserInfoResponse> obtainUserInfo(int userId);
}
